module.exports = function(config) {
  return config.set({
    // 下面files里的基础目录
    basePath: '../',
    // 应用的测试框架
    frameworks: ['jasmine', 'requirejs'],
    // 测试环境需要加载的JS信息
    files: [{
      pattern: 'src/libs/**/*.js',
      included: false
    }, {
      pattern: 'src/scripts/**/*.js',
      included: false
    }, {
      pattern: 'test/**/*_test.js',
      included: false
    }, 'test/require.conf.js'],
    exclude: ['src/scripts/config.js'],
    preprocessors: {
      "**/*.coffee": ["coffee"]
    },
    coffeePreprocessor: {
      options: {
        bare: true,
        sourceMap: false
      },
      transformPath: function(path) {
        return path.replace(/\.coffee$/, '.js');
      }
    },
    preprocessors: {
      '../src/scripts/**/*.js': ['coverage'],
      'test/specJs/**/*.spec.js': ['coverage']
    },
    // 测试内容的输出以及导出用的模块名
    reporters: ['progress', 'coverage'],
    // 设置输出测试内容文件的信息(覆盖)
    coverageReporter: {
      type: 'html',
      dir: 'test/coverage/'
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    // 是否自动监听上面文件的改变自动运行测试
    autoWatch: false,
    // 用什么环境测试代码,这里是chrome`
    browsers: ['Chrome'],
    // 用到的插件,比如chrome浏览器与jasmine插件
    plugins: ['karma-jasmine', 'karma-chrome-launcher', 'karma-coverage', 'karma-coffee-preprocessor', 'karma-requirejs'],
    singleRun: false
  });
};