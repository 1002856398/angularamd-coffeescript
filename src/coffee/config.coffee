require.config
	baseUrl: './scripts'
	urlArgs: 'v=1.0.0'

	paths:
		angular: '../libs/angular/angular'
		'angular-mocks': '../libs/angular-mocks/angular-mocks'
		'angular-ui-router': '../libs/angular-ui-router/release/angular-ui-router'
		angularAMD: '../libs/angularAMD/angularAMD'
		ngload: '../libs/angularAMD/ngload'

	shim:
		'angular-mocks': ['angular']
		'angular-ui-router': ['angular']
		angularAMD: ['angular']
		ngload: ['angularAMD']

	deps: ['app']